<?php
	// C'EST UN ÉNORME FATRAS OKAY ?

	// config:
	$PASSWORD='YOURPASSWORD';
	$SUBDIR='files';
	$scriptname = basename($_SERVER["SCRIPT_NAME"]);
	date_default_timezone_set('Europe/Paris'); // because french baguette omelette du fromage


	// disconnect
	if(isset($_GET['deco'])){
		setcookie('gné', '',time()-5);
		setcookie("expire","a",time()-5);
		header('Location: index.php');
	}

	// connect
	if(isset($_POST['password']) && $_POST['password'] == $PASSWORD){
		setcookie('gné', $PASSWORD, time()+60*60*2); // not very secure ?
		setcookie("expire",time()+60*60*2,time()+60*60*2);
	}

	if(isset($_GET["hostImage"])){

		// false password.
		if(!isset($_COOKIE['gné'])){
			if($_POST['password'] != $PASSWORD){
				print 'Mauvais mot de passe.';
				exit();
			}
		}

		$img = $_POST["image"];
		$img = str_replace('data:image/png;base64,', '', $img);
		$img = str_replace(' ', '+', $img);
		$data = base64_decode($img);
		$file_name = time(). '.png';
		$file_path = realpath("./". $SUBDIR ."/");
		$file_path = $file_path . "/" . $file_name ;

		if(file_put_contents($file_path , $data)) {
			 echo $SUBDIR ."/". $file_name;
		} else{
			echo "error!";
		}
		exit();
	}

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title>Up</title>
	<meta name="viewport" content="width=device-width" />
	<link rel="stylesheet" href="design.css" />
</head>
<body id="pasteArea">
	<h1>Hébergeur hypersimple <?php if(isset($_COOKIE['gné'])){ echo "<sup><a href=\"?deco\">X</a></sup>"; } ?></h1>

	<?php 
			// A simple, minimalist, personal file/image hosting script. - version 0.6
			// Only you can upload a file or image, using the password ($PASSWORD).
			// Anyone can see the images or download the files.
			// Files are stored in a subdirectory (see $SUBDIR).
			// This script is public domain.
			// Source: http://sebsauvage.net/wiki/doku.php?id=php:imagehosting
			// Added by Corentin Bettiol : list of files in the $SUBDIR subdirectory, 2h cookie connexion memory, including timestamp in the filenames, hosting images in clipboard by pressing ctrl+v.


	if(isset($_FILES['filetoupload']))
	{
		// sleep(3); // Reduce brute-force attack effectiveness. You can activate it if you have experienced attacks on your server.

		// false password.
		if(!isset($_COOKIE['gné'])){
			if($_POST['password'] != $PASSWORD){
				print 'Mauvais mot de passe.';
				exit();
			}
		}

		// timestamp in name file
		$filename = $SUBDIR.'/'. time() .'-'. basename($_FILES['filetoupload']['name']);

		// timestamp should prevent this
		if(file_exists($filename)){
			print 'This file already exists.'; exit();
		}

		// hosting the file and displaying the url
		if(move_uploaded_file($_FILES['filetoupload']['tmp_name'], $filename))
		{
			$serverport='';
			
			if ($_SERVER["SERVER_PORT"]!='80') {
				$serverport=':'.$_SERVER["SERVER_PORT"];
			}
			
			$fileurl='http://'.$_SERVER["SERVER_NAME"].$serverport.dirname($_SERVER["SCRIPT_NAME"]).'/'.$filename;
			
			echo 'Le fichier a bien été hébergé (url : <a href="'.$fileurl.'">'.$fileurl.'</a>).';
		}

		// error during the hosting process
		else{
			echo "Il y a eu une erreur d'hébergement du fichier, réessayez svp.";
		}

		echo '<br><br><a href="'.$scriptname.'">Héberger un autre fichier</a>/<a href="'. $SUBDIR .'/index.php">Voir les fichiers hébergés</a>';
		
		exit();
	}
	?>

	<form method="post" action="index.php" enctype="multipart/form-data">        
		<label>Fichier :</label><input type="file" name="filetoupload" size="60">
		<input type="hidden" name="MAX_FILE_SIZE" value="512000000"><br>
			<label <?php if(isset($_COOKIE['gné'])){ echo "style=\"display: none;\""; } ?>>Mot de passe :</label><input type="password" id="password" name="password" <?php if(isset($_COOKIE['gné'])){ echo "style=\"display: none;\""; } ?>><br>
		<label></label><input type="submit" value="ENVOYER DANS L'ESPACE"> 
	</form>

	<p id="hostedImage">

	</p>

	<p>
		<a href="<?php echo $SUBDIR; ?>/index.php">Voir les fichiers hébergés</a>.
	</p>
	<p>
		<small>script d'auto-hébergement de fichier par <a href="http://sebsauvage.net/wiki/doku.php?id=php:filehosting">sebsauvage.net</a>, modifié par <a href="http://l3m.in/">l3m.in</a> (fork me on <a href="https://gitlab.com/sodimel/up">GitLab</a>!)</small>
	</p>


	<?php if(isset($_COOKIE['gné']) && $_COOKIE['gné'] ==$PASSWORD){ echo "<p><small>Autodéco dans ". date("H:i:s", $_COOKIE['expire']-time()) .".</small></p>"; } ?>

	<script>
		document.getElementById('pasteArea').onpaste = function (event) {

			document.getElementById("hostedImage").innerHTML = "Hébergement en cours...";

			// use event.originalEvent.clipboard for newer chrome versions
			var items = (event.clipboardData  || event.originalEvent.clipboardData).items;
			console.log(JSON.stringify(items)); // will give you the mime types
			// find pasted image among pasted items
			var blob = null;
			for (var i = 0; i < items.length; i++) {
				if (items[i].type.indexOf("image") === 0) {
					blob = items[i].getAsFile();
				}
			}
			// load image if there is a pasted image
			if (blob !== null) {
				var reader = new FileReader();
				reader.onload = function(event) {
					console.log(event.target.result); // data url !

					// thanks https://stackoverflow.com/a/30845034 !
					var http = new XMLHttpRequest();
					var url = "index.php?hostImage";
					var pass = "";
					if(typeof document.getElementById("password").value !== "undefined")
						pass = document.getElementById("password").value;
					var params = "image="+ event.target.result + "&password=" + pass;
					http.open("POST", url, true);

					//Send the proper header information along with the request
					http.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

					http.onreadystatechange = function() {//Call a function when the state changes.
						if(http.readyState == 4 && http.status == 200) {
							if(http.responseText != "Mauvais mot de passe.")
								document.getElementById("hostedImage").innerHTML = "L'image a bien été hébergée: <a href=\""+ http.responseText +"\">"+ http.responseText +"</a>";
							else
								document.getElementById("hostedImage").innerHTML = "L'image n'a pas été hébergée: Mauvais mot de passe.";
						}
					}
					http.send(params);


				};
				reader.readAsDataURL(blob);
			}
		}
	</script>

</body>
</html>